<?php
declare(strict_types=1);

namespace app\tests\unit\genericExercises;

use Codeception\Specify;
use Codeception\Test\Unit;

class BrandConfiguratorTest extends Unit
{
    use Specify;

    public function testGenerateOffer()
    {
        $this->specify('It should build the right offer', function () {
            /* @var \app\common\components\BrandConfigurator $brandConfigurator */
            $brandConfigurator = \Yii::$app->brandConfigurator;

            $carOffer = $brandConfigurator->generateOffer();

            $this->assertEquals('Toyota', $carOffer->getCar()->getBrand());
            $this->assertEquals('Corolla', $carOffer->getCar()->getModel());
            $this->assertEquals('Some Dealer', $carOffer->getDealerName());
        });
    }
}