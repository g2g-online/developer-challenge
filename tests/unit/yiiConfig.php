<?php

return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    [
        'id' => 'app-unit-tests-common',
        'basePath' => dirname(__DIR__),
        'components' => [
            'log' => [
                'traceLevel' => 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning', 'info'],
                        'logVars' => [],
                    ]
                ],
            ],
        ]
    ]
);
