<?php
declare(strict_types=1);

namespace app\tests\unit\genericExercises;

use app\common\genericExercises\BasicExercises;
use Codeception\Specify;
use Codeception\Test\Unit;

class BasicExercisesTest extends Unit
{
    use Specify;

    public function testAddValue()
    {
        $initialValue = 2;
        $valueToAdd = 3;

        BasicExercises::addValue($initialValue, $valueToAdd);
        $this->assertEquals(5, $initialValue);
    }

    public function testFindMostFrequentElement()
    {
        $this->specify('it should find the majority element in an int list', function () {
           $elements = [1, 3, 5, 3, 7, 5, 3, 6, 2];

           $majorityElement = BasicExercises::findMostFrequentElement($elements);
           $this->assertEquals(3, $majorityElement);
        });

        $this->specify('it should find the majority element in a string list', function () {
            $elements = ['testing', 'did', 'done', 'check', 'dev', 'operator', 'dev', 'kind', 'test'];

            $majorityElement = BasicExercises::findMostFrequentElement($elements);
            $this->assertEquals('dev', $majorityElement);
        });

        $this->specify('it should find the majority element in a combined list', function () {
            $elements = [1, 1, 5, 'check', 7, 22, 3, 6, 'test'];

            $majorityElement = BasicExercises::findMostFrequentElement($elements);
            $this->assertEquals(1, $majorityElement);
        });
    }

    public function testGenerateFibonacciSequence()
    {
        $number = BasicExercises::generateFibonacciSequence(14);
        $this->assertEquals(377, $number);

        $number = BasicExercises::generateFibonacciSequence(50);
        $this->assertEquals(12586269025, $number);
    }
}