<?php
declare(strict_types=1);

use yii\BaseYii;

/**
 * Used for Yii::$app-><component_name> autocompletion
 */
class Yii extends BaseYii
{
    /** @var BaseApplication|WebApplication|ConsoleApplication */
    public static $app;
}

abstract class BaseApplication extends yii\base\Application {}

class WebApplication extends yii\web\Application {}

class ConsoleApplication extends yii\console\Application {}