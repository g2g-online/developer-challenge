<?php
declare(strict_types=1);

namespace app\common\components;

use app\common\models\Car;
use yii\base\Component;

class CarFactory extends Component
{
    public string $carToBeBuilt;

    private array $carToBeBuiltFunctionMapping = [
        'toyotaCorolla' => 'buildToyotaCorolla',
        'audiA3' => 'buildAudiA3'
    ];

    /**
     * Depending on the value of the $carToBeBuilt class param, this function should call the appropriate method
     * in the $carToBeBuiltFunctionMapping
     * @example: For $carToBeBuilt='toyotaCorolla', the buildCar() method should call the buildToyotaCorolla() method
     *
     * @return Car
     */
    public function buildCar(): Car
    {
        // TODO: Implement this function correctly, instead of randomly instatiating a Car
        return new Car();
    }

    public function buildToyotaCorolla(): Car
    {
        return (new Car())
            ->setBrand('Toyota')
            ->setModel('Corolla')
            ->setEngineCapacity(1.8)
            ->setNumberOfDoors(4);
    }

    public function buildAudiA3(): Car
    {
        return (new Car())
            ->setBrand('Audi')
            ->setModel('A3')
            ->setEngineCapacity(2.0)
            ->setNumberOfDoors(5);
    }
}