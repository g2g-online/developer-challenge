<?php
declare(strict_types=1);

namespace app\common\components;

use app\common\models\CarOffer;
use yii\base\Component;

class BrandConfigurator extends Component
{
    private CarFactory $carFactory;

    private OfferFactory $offerFactory;

    /**
     * BrandConfigurator constructor. You might wanna take a closer look at this constructor. There's a lot of cool
     * things you can do with a constructor, maybe you can do something here as well :)
     *
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * Based on the car built by the CarFactory, builds an offer using the offertFactory
     *
     * @return CarOffer
     */
    public function generateOffer(): CarOffer
    {
        $car = $this->carFactory->buildCar();

        return $this->offerFactory->generateOffer($car);
    }
}