<?php
declare(strict_types=1);

namespace app\common\components;


use app\common\models\Car;
use app\common\models\CarOffer;

class OfferFactory
{
    public string $dealerName;

    public float $dealerPrice;

    public function generateOffer(Car $car): CarOffer
    {
        return (new CarOffer)
            ->setCar($car)
            ->setListPrice($this->dealerPrice)
            ->setDealerName($this->dealerName);
    }
}