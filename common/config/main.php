<?php

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'container' => [
        'singletons' => [
            'app\common\components\CarFactory' => [
                'carToBeBuilt' => 'toyotaCorolla'
            ],
            'app\common\components\OfferFactory' => [
                'dealerName' => 'Some Dealer',
                'dealerPrice' => 15000.5
            ]
        ]
    ],
    'components' => [
        'brandConfigurator' => 'app\common\components\BrandConfigurator'
    ]
];
