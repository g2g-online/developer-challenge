<?php
declare(strict_types=1);

namespace app\common\genericExercises;

class BasicExercises
{
    /**
     * This function should be able to modify directly the value of the $number and the $valueToAdd.
     *
     * @example For an initial value $a = 2, calling $addValue($a, 3) should result in the altering of $a = 5
     *
     * @param int $number
     * @param int $valueToAdd
     */
    public static function addValue(int $number, int $valueToAdd): void
    {
        // TODO: Implement method
    }

    /**
     * Given an array of int and string, this function should return the element that occurs the most times
     *
     * @param $elements
     * @return int|string The most frequent element
     */
    public static function findMostFrequentElement(array $elements)
    {
        return '';
    }

    /**
     * Given the parameter $n, fetch the n-th number from the fibonacci sequence.
     *
     * NOTE: Extra points are given for efficiency here! :)
     *
     * @param int $n
     * @return int The n-th fibonacci number
     */
    public static function generateFibonacciSequence(int $n): int
    {
        return 0;
    }
}