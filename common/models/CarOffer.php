<?php


namespace app\common\models;


class CarOffer
{
    private Car $car;

    private float $listPrice;

    private string $dealerName;

    /**
     * @return Car
     */
    public function getCar(): Car
    {
        return $this->car;
    }

    /**
     * @param Car $car
     */
    public function setCar(Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    /**
     * @return float
     */
    public function getListPrice(): float
    {
        return $this->listPrice;
    }

    /**
     * @param float $listPrice
     */
    public function setListPrice(float $listPrice): self
    {
        $this->listPrice = $listPrice;

        return $this;
    }

    /**
     * @return string
     */
    public function getDealerName(): string
    {
        return $this->dealerName;
    }

    /**
     * @param string $dealerName
     */
    public function setDealerName(string $dealerName): self
    {
        $this->dealerName = $dealerName;

        return $this;
    }
}