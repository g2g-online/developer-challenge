<?php
declare(strict_types=1);

namespace app\common\models;

class Car
{
    private float $engineCapacity;

    private int $numberOfDoors;

    private string $brand;

    private string $model;

    /**
     * @return float
     */
    public function getEngineCapacity(): float
    {
        return $this->engineCapacity;
    }

    /**
     * @param float $engineCapacity
     */
    public function setEngineCapacity(float $engineCapacity): self
    {
        $this->engineCapacity = $engineCapacity;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfDoors(): int
    {
        return $this->numberOfDoors;
    }

    /**
     * @param int $numberOfDoors
     */
    public function setNumberOfDoors(int $numberOfDoors): self
    {
        $this->numberOfDoors = $numberOfDoors;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }
}