# G2G Online Developer Challenge

#### Welcome to the G2G Online Coding Challenge!
In order to prove that you are indeed a  good fit with our company, we have devised this challenge
in order to verify the knowledge and problem solving skills of our candidates.

The challenge consist of 2 parts:
* A set of questions that are meant to test your general knowledge, problem solving skills and ability to design
an architecture that can solve the problem at hand
* A couple of PHP classes and methods which have to be modified in order to fulfill our test suite.

### Part 1

#### Case 1

The first case is about designing a functionality that can speed up the way we track users 
throughout our websites. Consider the following:

* Every time a person enters a web page, a session with his data is being registered. We
store things like IP, timestamp, a unique client ID that we generate and a list of visited pages.
* We have a __Session Processor__ that is able to process each session one by one and send it
forward through the pipeline
* Under normal usage (traffic), the session processor can keep up with the incoming sessions.
However, some times the traffic is too much to handle and the processors stops dropping sessions.

For clarity, a diagram depicting the process is also displayed below. 

![case1picture](images/case_1.png)

##### The question is:
* What architecture / component / magic can we use to make sure that under any conditions, 
no sessions are lost?

#### Case 2

* Inside the Session Processor shown above, we want to make sure we reject sessions coming
from specific sources. The sources are stored in a Google Sheet that can be easily edited
by our employees at any time. 

* This sheet acts as a "blacklist". Whenever a session comes in, we cross-check it against 
that blacklist and block it if it's from a source on the list. To fetch the blacklist,
we have to always call the Sheets API to get it. **However, the overhead of connecting to the
Google Sheets API to fetch the list is significant and it makes the whole system slow at times**.

##### The question is:
* Given that several sessions are being registered every second, how would you implement the
filtering service that ensures that only sessions that are not coming from the banned sources 
are coming in? If querying the API every time is not a solution, how can you make sure
that the list is still up-to-date if some employee decides to update it?

#### Case 3

We are not thinking of designing a dashboard to monitor the health of all of our systems.
One part of it is to make sure our system is in sync with the back-office of our partner
clients. 

More specifically, we want to see if the client sees a similar number of transactions as we do.
However, the numbers don’t always match up exactly (due to different filtering criteria) so we
can’t rely on exact matching. 

##### The question is:
Given that we have at our disposal transaction data for the last 30 days split by hour, so we're
able to see the trends on the volume of transactions every hour, how can we detect mathematically
whether the systems were in sync one hour ago? It is also worth mentioning that we do not usually
see big fluctuations from one day to another (if 7 days ago at noon we have received around 50
transactions, it is likely that will happen today as well).

#### Case 4

We have recently designed a map visualization to enable our customers to see the number of 
equipment installations we have per region / per country. 

You can find it [here](https://www.practicomfort.fr/solutions-produits/monte-escalier-solution) 
by scrolling down to the interactive map. You can click on different regions and it will take
you to a more granular split into sub-regions. 

In the end, at the most granular point, you will see there are different **circles** with numbers
inside in the sub-region. Those are groups of multiple installations

Although we have the exact GPS coordinates of each installation, we decided to group them
and display them in this circle form due to privacy concerns (the group size must be at least 3
to avoid exact detection). 

##### The question is:

How would you group these GPS coordinates together to make sure that the groups we are
generating have sense from a geographical standpoint (eg. we don't want to group installations
that are too far apart into the same group). Are you aware of any algorithms that would be
able to achieve this automatically for us?

### Part 2

You will notice that in the repository, there is a Yii2 project already configured and a 
pre-installed codeception test suite. To be able to start working on the project though,
some basic knowledge of Docker is required. We assume you already have knowledge of that,
so we will not detail the exact commands that you need to type in order to spin up the project,
but rather the steps in natural language.

#### How to startup the project
1. You build the Docker container and spin it up using the provided docker-compose file.
2. You `bash` into the challenge-workspace container and install the Composer dependencies.
3. You test whether everything is installed properly by running `php vendor/bin/codecept run`.
At this point, if the tests just fail and you get no other error, that means you have
successfully booted up the project.

#### The (fun) coding part:

##### The Basic exercises
* In `common/genericExercises` there is a class named `BasicExercises`. In there, you will
notice 3 methods that have 3 different goals. Each one is documented in order to describe
the functionality that needs to be implemented. If you solve them correctly, the tests
in `tests/unit/genericExercises/BasicExercisesTest` should all be solved correctly.

##### The Car Brand Configurator Problem
* The other functionality we are aiming to build is to construct a Brand Configurator component.
* To achieve this, we have 2 models: `Car` and `CarOffer`. One holds the specs of the car,
one a possible offer from a car dealership.
* There are 3 `components` that are used in the configurator.
* The `CarFactory` builds the actual car, depending on the `$carToBeBuilt` parameter, which
you should figure out how it is magically instantiated outside of the class. We expect you
to also be able to fill in the `buildCar()` method, as described in the documentation.
* The `OfferFactory` builds the offer, using the Car from the `CarFactory`. This class
also has 2 magic class parameters, `dealerName` and `dealerPrice`. This class does not
necessarily need any changes. 
* The `BrandConfigurator` is the class that builds the configuration. You will notice
that this class also has 2 magic parameters: `$carFactory` and `$offerFactory`. These
2 parameters are not magically instantiated any more and it is up to you to do it, in what
way you see fit.

#### First, let's answer some questions:
1. Where are the parameters of `CarFactory` and `OfferFactory` magically instantiated and
how does this procedure work? (Note: Knowledge of Yii2 is required)
2. If everything goes well, you will notice that the car being built is a `Toyota Corolla`.
However, our `CarFactory` also has an assembly line for an Audi A3. How can we tweak our
project so that we build Audi's instead?

#### Now down to the coding part:
What you need to do is the following:
* Implement the `buildCar` method in the `CarFactory` and configure the `BrandConfigurator`
so that the configuration is generated properly.
* If everything does well, you will see the `tests/unit/components/BrandConfiguratorTest.php`
performs the test successfully. 